# ITCNE23 - MSVC - Lernumgebung

hier finden sich alle Details zur spezifischen Durchführung des Moduls MSVC im 3. Semester.

## Lektionenplan
die folgende Tabelle stellt einen groben Fahrplan für das Modul dar. Der tatsächliche Ablauf kann leicht abweichen.

| Tag | Datum | Thema | Lektionen | Anm. |
| ---: | ---: | --- | --- | --- |
| 1 | 21.02.24 | Kennenlernen, Einführung in das Thema, Arbeitsumgebung, Minimum Flask Server | 5 (NaMi) | - |
| 2 | 28.02.24 | Minimum Flask Sever fertig, Docker Compose, REST | 4 (VoMi) | - |
| 3 | 06.03.24 | CRUD, SQLAlchemy, Anbindung mySQL | 4 (VoMi) | - |
| 4 | 13.03.24 | On the way to the Cloud (AWS), Skalierbare Projektstruktur, Testing, Production Env, GitLab Flow | 9 | - |
| 5 | 20.03.24 | GitLab CI, Intro MicroAdventures | 4 (VoMi) | - |
| 6 | 27.03.24 | Pattern - Security (Token, Auth) - Teil 1| 4 (VoMi) | - |
| 7 | 03.04.24 | Pattern - Security (Token, Auth) - Teil 2 | 4 (VoMi) | - |
| 8 | 10.04.24 | Pattern - Logging, Error handling and Monitoring  | 4 (VoMi) | - |
| 9 | 17.04.24 | Pattern - Async vs Sync Communication | 4 (VoMi) | - |
| - | 24.04.24 | Frühlingsferien
| - | 31.04.24 | Frühlingsferien
| 10 | 08.05.24 | Scrum Setup, GitLab Fork Update | 3 (NaMi) | - |
| 11 | 15.05.24 | UI MS-patterns | 4 (VoMi) | - |
| - | 22.05.24 | kein Microservices Modul
| 12 | 29.05.24 | microservices.io - patterns | 4 (VoMi) | - |
| 13 | 05.06.24 | Pattern - API Gateway and graphQL | 4 (VoMi) | - |
| 14 | 12.06.24 | GraphQL Vertiefung | 4 (VoMi) | - |
| 15 | 19.06.24 | Asynchrone Kommunikation & Redis | 4 (VoMi) | - |
| 16 | 26.06.24 | Service Discovery | 4 (VoMi) | - |
| 17 | 03.07.24 | TBD | 4 (VoMi) | - |
| 18 | 10.07.24 | TBD | 5 (NaMi) | - | 


# Logbuch
hier sind wichtigen Infos und Links zu den bearbeiteten Themen abgelegt. *Achtung*: Der neueste Eintrag steht oben.


### Tag 16 - 26.06.2024

- Pattern __Service Discovery__
    - https://www.baeldung.com/cs/service-discovery-microservices
    - Skaffold-Übung: https://codelabs.developers.google.com/understanding-skaffold#0

### Tag 15 - 19.06.2024

- Scrum Retro
- [Asynchrone Kommunikation](https://gitlab.com/ch-tbz-wb/Stud/MSVC/-/blob/main/2_Unterrichtsressourcen/D_Advanced_Topics/06_ms_asynchronous_communication.md?ref_type=heads)
- Micro Adventures Room

### Tag 14 - 12.06.2024

- Fortsetzung GraphQL

### Tag 13 - 05.06.2024

- Exkurs - Requirements Management and Design Thinking
- [API Gateways](https://gitlab.com/ch-tbz-wb/Stud/MSVC/-/blob/main/2_Unterrichtsressourcen/D_Advanced_Topics/06_api_gateways_graphQL.md?ref_type=heads)
- graphQL

### Tag 12 - 29.05.2024

- Scrum - Daily Meeting Micro Adventures
- Wrap Up [Microservice patterns](https://gitlab.com/ch-tbz-wb/Stud/MSVC/-/blob/main/2_Unterrichtsressourcen/D_Advanced_Topics/05_ms_patterns.md?ref_type=heads)
- Arbeit an Issues

### Tag 11 - 15.05.2024

- Scrum - Daily Meeting Micro Adventures
- Input - [UI patterns](https://gitlab.com/ch-tbz-wb/Stud/MSVC/-/blob/main/2_Unterrichtsressourcen/D_Advanced_Topics/04_ms_ui_patterns.md?ref_type=heads) - SSR vs. SPA 
- Arbeit an Issues
- Besprechungen Semesterarbeit


### Tag 10 - 08.05.2024

- Dev Learnings
- Micro Adventures - Room Development and Planning
- Besprechungen Semesterarbeit

### Tag 9 - 17.04.2024

- [Micro Adventures Intro](https://gitlab.com/ch-tbz-wb/Stud/MSVC/-/blob/main/2_Unterrichtsressourcen/D_Advanced_Topics/03_micro_adventures.md?ref_type=heads)

### Tag 8 - 10.04.2024

- [Logging und Monitoring](https://gitlab.com/ch-tbz-wb/Stud/MSVC/-/blob/main/2_Unterrichtsressourcen/D_Advanced_Topics/02_monitoring.md?ref_type=heads) von Microservices.
- Für alle, die aufholen müssen: ihr könnt auf [diesem Repo](https://gitlab.com/tbz-itcne23-msvc/blueprint-flask-prod/-/tree/auth?ref_type=heads) aufsetzten (Branch "auth")
- Wiederholung

### Tag 7 - 03.04.2024

- Teil 2 Pattern - [Authentifizierung und Autorisierung mit Tokens](https://gitlab.com/ch-tbz-wb/Stud/MSVC/-/blob/main/2_Unterrichtsressourcen/D_Advanced_Topics/01_authorization.md?ref_type=heads)
- Planning Meeting - Micro Adventures

### Tag 6 - 27.03.2024

- Pattern - [Authentifizierung und Autorisierung mit Tokens](https://gitlab.com/ch-tbz-wb/Stud/MSVC/-/blob/main/2_Unterrichtsressourcen/D_Advanced_Topics/01_authorization.md?ref_type=heads)


### Tag 5 - 20.03.2024

- Manuelles [Deployment auf EC2](https://gitlab.com/ch-tbz-wb/Stud/MSVC/-/blob/main/2_Unterrichtsressourcen/C_Towards_Production/03_manual_deployment.md?ref_type=heads) fertig machen
- Unser Entwicklungsprozess - [GitLab Flow](https://gitlab.com/ch-tbz-wb/Stud/MSVC/-/blob/main/2_Unterrichtsressourcen/C_Towards_Production/04_gitlab_flow.md?ref_type=heads)
- Automatisierung - GitLab CI - [wir bauen eine eigene Pipeline](https://gitlab.com/ch-tbz-wb/Stud/MSVC/-/blob/main/2_Unterrichtsressourcen/C_Towards_Production/05_automation.md?ref_type=heads)

### Tag 4 - 13.03.2024

- Many to Many Relationship fertig machen - Besprechung Beispiel - [Persistenz mit SQLAlchemy](https://gitlab.com/ch-tbz-wb/Stud/MSVC/-/blob/main/2_Unterrichtsressourcen/B_Microservices_lokal/03_persistence_with_mysql.md?ref_type=heads)
- [Skalierbare MicroService Struktur mit Blueprints](https://gitlab.com/ch-tbz-wb/Stud/MSVC/-/blob/main/2_Unterrichtsressourcen/B_Microservices_lokal/04_scalable_structure.md?ref_type=heads) 
- [Testing](https://gitlab.com/ch-tbz-wb/Stud/MSVC/-/blob/main/2_Unterrichtsressourcen/C_Towards_Production/01_testing.md?ref_type=heads) - Grundlagen und PyTest
- [Production Umgebung](https://gitlab.com/ch-tbz-wb/Stud/MSVC/-/blob/main/2_Unterrichtsressourcen/C_Towards_Production/02_production_image.md?ref_type=heads)

Hausaufgaben:
- [EC2 Instanz Vorbereiten](https://gitlab.com/ch-tbz-wb/Stud/MSVC/-/blob/main/2_Unterrichtsressourcen/C_Towards_Production/03_manual_deployment.md?ref_type=heads) - "bis Verbindung per SSH herstellen"


### Tag 3 - 06.03.2024

- [Persistenz mit SQLAlchemy](https://gitlab.com/ch-tbz-wb/Stud/MSVC/-/blob/main/2_Unterrichtsressourcen/B_Microservices_lokal/03_persistence_with_mysql.md?ref_type=heads) - CRUD und REST


### Tag 2 - 27.02.2024

- [minimal Flask](https://gitlab.com/ch-tbz-wb/Stud/MSVC/-/blob/main/2_Unterrichtsressourcen/B_Microservices_lokal/01_minimal_dockerized_flask.md?ref_type=heads) fertig machen. Hot-reload der Lehrperson demonstrieren.
- [REST APIs](https://gitlab.com/ch-tbz-wb/Stud/MSVC/-/blob/main/2_Unterrichtsressourcen/B_Microservices_lokal/02_restful_api_postman.md?ref_type=heads)
- 5-Finger Feedback Nr. 1 [MS-Forms](https://forms.office.com/Pages/ResponsePage.aspx?id=xf0z91USjU23kyvHrKDyFNOekXfhs2pDkFCW7GJ77p1UMkJORFdVRUhDT1VRWjdKMEQ2WlhXWjRPRi4u)


### Tag 1 - 21.02.2024

- Kennenlernen
- [Erwartungen abklären](https://teams.microsoft.com/l/entity/95de633a-083e-42f5-b444-a4295d8e9314/_djb2_msteams_prefix_894156181?context=%7B%22channelId%22%3A%2219%3AF6JhZoyZdn8_XROVWovrTx2ibB1ECs99UH-tuRtGwPc1%40thread.tacv2%22%7D&tenantId=f733fdc5-1255-4d8d-b793-2bc7aca0f214)
- [Einführung Microservices](https://gitlab.com/ch-tbz-wb/Stud/MSVC/-/tree/main/2_Unterrichtsressourcen/0_Einf%C3%BChrung?ref_type=heads)
- [Entwicklungsumgebung aufsetzen](https://gitlab.com/ch-tbz-wb/Stud/MSVC/-/tree/main/2_Unterrichtsressourcen/A_Entwicklungsumgebung?ref_type=heads)
- [erste Schritte mit Flask](https://gitlab.com/ch-tbz-wb/Stud/MSVC/-/blob/main/2_Unterrichtsressourcen/B_Microservices_lokal/01_minimal_dockerized_flask.md?ref_type=heads)

